<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Debit extends Model
{
    protected $fillable = ['debit_date','debit_amount','debit_remarks'];
}
