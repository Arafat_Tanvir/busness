<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Head;
use App\Models\Credit;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $credits =Credit::orderBy('id','desc')->get();
        $heads =Head::orderBy('id','desc')->get();
        return view('backend.index',[
            'heads'=>$heads,
            'credits'=>$credits
        ]);
    }

    public function home()
    {
        $credits =Credit::orderBy('id','desc')->get();
        $heads =Head::orderBy('id','desc')->get();
        return view('backend.index',[
            'heads'=>$heads,
            'credits'=>$credits
        ]);
    }
}
