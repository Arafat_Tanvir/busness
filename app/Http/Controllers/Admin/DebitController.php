<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Debit;
use App\Models\Head;

class DebitController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
	

	public function search_bydebit_two_date(Request $request){
		$this->validate($request,[
			'from_date'=>'required',
			'to_date'=>'required'
		]);

		$debits=Debit::whereBetween('debit_date', [$request->from_date, $request->to_date])->get();
		
		$heads =Head::orderBy('id','desc')->get();
		

		return view('backend.debits.debit_report',[
			'heads'=>$heads,
			'debits'=>$debits
		]);
	}

	public function search_debit_two_date(){
		return view('backend.debits.search_two_date');
	}

    public function index()
	{
	    $debits =Debit::orderBy('id','desc')->get();
	    return view('backend.debits.index',compact('debits'));
	}

	public function create()
	{
	    return view('backend.debits.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'debit_date'=>'required',
			'debit_amount'=>'required|numeric',
			'debit_remarks'=>'required',
		]);

		$debits=new Debit();
		$debits->debit_date=$request->debit_date;
		$debits->debit_amount=$request->debit_amount;
		$debits->debit_remarks=$request->debit_remarks;

		$debits->save();
		if(!is_null($debits)){
			session()->flash('success','debit create Successfully!!');
			return redirect()->route('debit_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}

	}


	public function show($id)
	{
		$debits=Debit::findOrFail($id);
		return view('backend.debits.show',compact('debits'));
	}



	public function edit($id)
	{
		$debit = Debit::findOrFail($id);
		if(!is_null($debit))
		{
			return view('backend.debits.edit',compact('debit'));
		}else
		{
			return redirect()->route('debit_index');
		}
	}


	public function update(Request $request, $id)
	{
       $this->validate($request,[
			'debit_date'=>'required',
			'debit_amount'=>'required|numeric',
			'debit_remarks'=>'required',
		]);

		$debit=Debit::findOrFail($id);
		$debit->debit_date=$request->debit_date;
		$debit->debit_amount=$request->debit_amount;
		$debit->debit_remarks=$request->debit_remarks;
		$debit->update();

		if(!is_null($debit)){
			session()->flash('success','debit create Successfully!!');
			return redirect()->route('debit_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
	
	public function delete($id)
	{
	    $debit=Debit::find($id);
	    if(!is_null($debit))
	    {
	        $debit->delete();
	        session()->flash('success','Account debit delete Successfully');
	        return back();
	    }else
	    {
	        session()->flash('stickly_error','Some Error Occer');
	        return back();
	    }

	}
}
