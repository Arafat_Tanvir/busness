<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Head;

class HeadController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
	{
	    $heads =Head::orderBy('id','desc')->get();
	    return view('backend.heads.index',compact('heads'));
	}

	public function create()
	{
	    return view('backend.heads.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'head_name'=>'required|max:50|min:2|unique:heads',
			'percentage'=>'required|max:5|min:1|numeric'
		]);

		$head_exits_data =Head::orderBy('id','desc')->get();
		$total_exit=0;
		foreach ($head_exits_data as $data) {
			$total_exit+=$data->percentage;
		}
		$total=$total_exit+$request->percentage;
		if ($total>100) {
			session()->flash('stickly_error','This Head Percentage is Greader Then 100%!!');
	        return redirect()->route('head_create');
		}else{

			$heads=new Head();
			$heads->head_name=$request->head_name;
			$heads->percentage=$request->percentage;
			$heads->save();
			if(!is_null($heads)){
				session()->flash('success','Head create Successfully!!');
				return redirect()->route('head_index');
			}else{
				session()->flash('stickly_error','Some Error Occer!!');
				return back();
			}

		}

	}


	public function show($id)
	{
		$heads=Head::findOrFail($id);
		return view('backend.heads.show',compact('heads'));
	}



	public function edit($id)
	{
		$head = Head::findOrFail($id);
		if(!is_null($head))
		{
			return view('backend.heads.edit',compact('head'));
		}else
		{
			return redirect()->route('head_index');
		}
	}


	public function update(Request $request, $id)
	{
		
        $exit_name=Head::where('head_name',$request->head_name)->where('id','!=',$id)->get();
       
        if(count($exit_name)>0)
	    {
	        session()->flash('stickly_error','This Head is Allready Exits!!');
	        return redirect()->route('head_index');
	    }else
	    {
	    	$head_exits_data =Head::orderBy('id','desc')->get();
			$total_exit=0;
			foreach ($head_exits_data as $data) {
				$total_exit+=$data->percentage;
			}
			$total=$total_exit+$request->percentage;
			if ($total>100) {
			session()->flash('stickly_error','This Head Percentage is Greader Then 100%!!');
	        return redirect()->route('head_create');
			}else{
		        $this->validate($request,[
					'head_name'=>'required|max:50|min:2',
					'percentage'=>'required|max:5|min:1|numeric'
				]);


		        $head=Head::findOrFail($id);
		        $head->head_name=$request->head_name;
				$head->percentage=$request->percentage;
				$head->update();

				if(!is_null($head)){
					session()->flash('success','Head create Successfully!!');
					return redirect()->route('head_index');
				}else{
					session()->flash('stickly_error','Some Error Occer!!');
					return back();
				}
			}
		}
	}
	
	public function delete($id)
	{
	    $head=Head::find($id);
	    if(!is_null($head))
	    {
	        $head->delete();
	        session()->flash('success','Account Head delete Successfully');
	        return back();
	    }else
	    {
	        session()->flash('stickly_error','Some Error Occer');
	        return back();
	    }

	}
}
