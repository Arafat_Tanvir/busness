<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Credit;
use App\Models\Head;

class CreditController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }


	public function search_bycredit_two_date(Request $request){
		$this->validate($request,[
			'from_date'=>'required',
			'to_date'=>'required'
		]);

		$credits=Credit::whereBetween('credit_date', [$request->from_date, $request->to_date])->get();
		
		$heads =Head::orderBy('id','desc')->get();
		

		return view('backend.credits.credit_report',[
			'heads'=>$heads,
			'credits'=>$credits
		]);
	}

	

	public function search_two_date(){
		return view('backend.credits.search_two_date');
	}



    public function index()
	{
	    $credits =Credit::orderBy('id','desc')->get();
	    return view('backend.credits.index',compact('credits'));
	}

	public function create()
	{
	    return view('backend.credits.create');
	}

	public function store(Request $request)
	{
		$this->validate($request,[
			'credit_date'=>'required',
			'credit_amount'=>'required|numeric',
			'credit_remarks'=>'required',
		]);

		$credits=new Credit();
		$credits->credit_date=$request->credit_date;
		$credits->credit_amount=$request->credit_amount;
		$credits->credit_remarks=$request->credit_remarks;

		$credits->save();
		if(!is_null($credits)){
			session()->flash('success','credit create Successfully!!');
			return redirect()->route('credit_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}

	}


	public function show($id)
	{
		$credits=Credit::findOrFail($id);
		return view('backend.credits.show',compact('credits'));
	}



	public function edit($id)
	{
		$credit = Credit::findOrFail($id);
		if(!is_null($credit))
		{
			return view('backend.credits.edit',compact('credit'));
		}else
		{
			return redirect()->route('credit_index');
		}
	}


	public function update(Request $request, $id)
	{
       $this->validate($request,[
			'credit_date'=>'required',
			'credit_amount'=>'required|numeric',
			'credit_remarks'=>'required',
		]);

		$credit=Credit::findOrFail($id);
		$credit->credit_date=$request->credit_date;
		$credit->credit_amount=$request->credit_amount;
		$credit->credit_remarks=$request->credit_remarks;
		$credit->update();

		if(!is_null($credit)){
			session()->flash('success','credit create Successfully!!');
			return redirect()->route('credit_index');
		}else{
			session()->flash('stickly_error','Some Error Occer!!');
			return back();
		}
	}
	
	public function delete($id)
	{
	    $credit=Credit::find($id);
	    if(!is_null($credit))
	    {
	        $credit->delete();
	        session()->flash('success','Account credit delete Successfully');
	        return back();
	    }else
	    {
	        session()->flash('stickly_error','Some Error Occer');
	        return back();
	    }

	}
}
