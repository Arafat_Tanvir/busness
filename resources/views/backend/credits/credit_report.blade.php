@extends('backend.layouts.master')

@section('content')
<div class="row">
	
	<div class="col-sm-12">
		
	
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Credit Report</h1>
            </div>

            <div class="card-body">
             
              <div class="mt-2">

              	<div class="table-responsive mt-2">
				        <table id="credit" class="table table-bordered table-striped">
				          <caption>Reports of Credits </caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Date</th>
				  						<th>Amount</th>
				  						@foreach($heads as $head)
				  						<th>{{ $head->head_name}}</th>
				  						@endforeach
				  						
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($credits as $credit)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $credit->credit_date }}</td>
				  						<td class="text-center">
				  						    @if($credit->credit_amount)
							                  <p>{{ $credit->credit_amount}}</p>
							                @else
							                    <p>N/A</p>
							                @endif
				  						</td>

				  						@foreach($heads as $head)
				  						
				  							@php

				  							$total=(($credit->credit_amount * $head->percentage)/100);

				  							@endphp

				  						<td>{{ $total }} ({{ $head->percentage}}%)</th>
				  						@endforeach
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
	      	
	    </div>
	  </div>

	</div>
	 <input type="button" class="btn btn-primary float-right" value="Print" onClick="window.print()">
	</div>
	
	</div>
	@endsection