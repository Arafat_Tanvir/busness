@extends('backend.layouts.master')

@section('content')
<div class="row">
	<div class="col-sm-2">
		
	</div>
	<div class="col-sm-8">
		
	
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Credit Add</h1>
            </div>

            <div class="card-body">
              <h1 class="text-right"><a href="{{route('credit_index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
	      	<form method="POST" class="user" action="{{ route('credit_store') }}">
	         {{ csrf_field()}}

	          <div class="form-group">
	              <label for="credit_date">Date</label>
	              <div class="form-input">
	                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="credit_date" id="credit_date" placeholder="Enter Credit Date" value="{{old('credit_date')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('credit_date')) ? $errors->first('credit_date') : ''}}
	                  </div>
	              </div>
	          </div>

	          <div class="form-group">
	              <label for="credit_amount">Amount</label>
	              <div class="form-input">
	                  <input type="float" class="form-control form-control-user is-valid form-control-sm" name="credit_amount" id="credit_amount" placeholder="Enter Credit Amount" value="{{old('credit_amount')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('credit_amount')) ? $errors->first('credit_amount') : ''}}
	                  </div>
	              </div>
	          </div>

	          <div class="form-group">
	              <label for="credit_remarks">Remarks</label>
	              <div class="form-input">
	                  <textarea name="credit_remarks" cols="4" rows="5" value="{{old('credit_remarks')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="credit_remarks" required >{{ old('credit_remarks')}}</textarea>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('credit_remarks')) ? $errors->first('credit_remarks') : ''}}
	                  </div>
	              </div>
	          </div>


	          <button class="btn btn-primary float-right" type="submit">Add Credit</button>
	      </form>
	    </div>
	  </div>
	</div>
	</div>
	<div class="col-sm-2">
		
	</div>
	</div>
	@endsection

@section('scripts')

	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

	<script>
	    $(function(){
	      $("#credit_date").datepicker({dateFormat: "yy-mm-dd"}).val();
	    });
	</script>
@endsection
