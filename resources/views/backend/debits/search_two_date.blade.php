@extends('backend.layouts.master')

@section('content')
<div class="row">
	<div class="col-sm-2">
		
	</div>
	<div class="col-sm-8">
		
	
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Search Debit Between Two Date</h1>
            </div>

            <div class="card-body">
	      	<form method="POST" class="user" action="{{ route('search_bydebit_two_date') }}">
	         {{ csrf_field()}}

	          <div class="form-group">
	              <label for="from_date">From</label>
	              <div class="form-input">
	                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="from_date" id="from_date" placeholder="Enter Credit Date" value="{{old('from_date')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('from_date')) ? $errors->first('from_date') : ''}}
	                  </div>
	              </div>
	          </div>

	          <div class="form-group">
	              <label for="to_date">To</label>
	              <div class="form-input">
	                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="to_date" id="to_date" placeholder="Enter Credit Date" value="{{old('to_date')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('to_date')) ? $errors->first('to_date') : ''}}
	                  </div>
	              </div>
	          </div>

	          


	          <button class="btn btn-primary float-right" type="submit">Search</button>
	      </form>
	    </div>
	  </div>
	</div>
	</div>
	<div class="col-sm-2">
		
	</div>
	</div>
	@endsection

@section('scripts')

	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

	<script>
	    $(function(){
	      $("#from_date").datepicker({dateFormat: "yy-mm-dd"}).val();
	      $("#to_date").datepicker({dateFormat: "yy-mm-dd"}).val();
	    });
	</script>
@endsection
