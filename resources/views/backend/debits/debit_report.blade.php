@extends('backend.layouts.master')

@section('content')
<div class="row">
	
	<div class="col-sm-12">
		
	
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Debit Report</h1>
            </div>

            <div class="card-body">
             
              <div class="mt-2">

              	<div class="table-responsive mt-2">
				        <table id="debit" class="table table-bordered table-striped">
				          <caption>Reports of Debits </caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Date</th>
				  						<th>Amount</th>
				  						
				  						
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($debits as $debit)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $debit->debit_date }}</td>
				  						<td class="text-center">{{ $debit->debit_amount }}</td>
				  						
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
	      	
	    </div>
	  </div>
	</div>
	 <input type="button" class="btn btn-primary float-right" value="Print" onClick="window.print()">
	</div>
	
	</div>
	@endsection