@extends('backend.layouts.master')

@section('content')
<div class="row">
	<div class="col-sm-2">
		
	</div>
	<div class="col-sm-8">
		
	
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Debit Update</h1>
            </div>

            <div class="card-body">
              <h1 class="text-right"><a href="{{route('debit_index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
	      	<form method="POST" class="user" action="{{ route('debit_update',$debit->id) }}">

	         <input type="hidden" name="_token" value="{{ csrf_token() }}">
	          <div class="form-group">
	              <label for="debit_date">Date</label>
	              <div class="form-input">
	                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="debit_date" id="debit_date" placeholder="Enter debit Date" value="{{ $debit->debit_date}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('debit_date')) ? $errors->first('debit_date') : ''}}
	                  </div>
	              </div>
	          </div>

	          <div class="form-group">
	              <label for="debit_amount">Amount</label>
	              <div class="form-input">
	                  <input type="float" class="form-control form-control-user is-valid form-control-sm" name="debit_amount" id="debit_amount" placeholder="Enter debit Amount" value="{{ $debit->debit_amount}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('debit_amount')) ? $errors->first('debit_amount') : ''}}
	                  </div>
	              </div>
	          </div>

	          <div class="form-group">
	              <label for="debit_remarks">Remarks</label>
	              <div class="form-input">
	                  <textarea name="debit_remarks" cols="4" rows="5" value="{{old('debit_remarks')}}" class="form-control form-control-user is-valid form-control-sm input-md" id="debit_remarks" required >{{ $debit->debit_remarks}}</textarea>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('debit_remarks')) ? $errors->first('debit_remarks') : ''}}
	                  </div>
	              </div>
	          </div>


	          <button class="btn btn-primary float-right" type="submit">Update debit</button>
	      </form>
	    </div>
	  </div>
	</div>
	</div>
	<div class="col-sm-2">
		
	</div>
	</div>
	@endsection

	@section('scripts')

	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

	<script>
	    $(function(){
	      $("#debit_date").datepicker({dateFormat: "yy-mm-dd"}).val();
	    });
	</script>
@endsection