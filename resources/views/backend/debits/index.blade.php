@extends('backend.layouts.master')

@section('content')
<div class="row">
	
	<div class="col-sm-12">
		
	
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Debits Information</h1>
            </div>

            <div class="card-body">
              <h1 class="text-right"><a href="{{route('debit_create')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">

              	<div class="table-responsive mt-2">
				        <table id="debit" class="table table-bordered table-striped">
				          <caption>List of debits </caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Date</th>
				  						<th>Amount</th>
				  						<th>Remarks</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($debits as $debit)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $debit->debit_date }}</td>
				  						<td class="text-center">
				  						    @if($debit->debit_amount)
							                  <p>{{ $debit->debit_amount}}</p>
							                @else
							                    <p>N/A</p>
							                @endif
				  						</td>
				  						<td>
				  							{{ $debit->debit_remarks}}
				  						</td>
				             
				            
				  						<td class="text-center"> <!-- <a href="{{route('debit_show', $debit->id)}}" class="btn btn-primary btn-sm">Show</a> -->
				  							<a href="{{route('debit_edit', $debit->id)}}" class="btn btn-warning btn-sm">Edit</a>
				                <a href="#DeleteModal{{ $debit->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$debit->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-debiter">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('debit_delete', $debit->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
	      	
	    </div>
	  </div>
	</div>
	</div>
	
	</div>
	@endsection