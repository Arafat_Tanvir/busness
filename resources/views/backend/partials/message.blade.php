<div class="container text-center" >
	<div class="row">
		<div class="col-sm-12">
			@if(Session::has('success'))
			<div class="alert alert-success Message" id="Message">
				<p>
					{{Session::get('success')}}
				</p>
			</div>
			@endif
		</div>
	</div>
</div>
<div class="container text-center">
	<div class="row">
		<div class="col-sm-12">
			@if(Session::has('stickly_error'))
			<div class="alert alert-danger Message" id="Message">
				<p>
					{{Session::get('stickly_error')}}
				</p>
			</div>
			@endif
		</div>
	</div>
</div>
