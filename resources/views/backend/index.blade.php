@extends('backend.layouts.master')

@section('content')

          <div class="row">
          	<canvas id="popChart" width="100%" height="45"></canvas>
          </div>
@endsection
@section('scripts')
          <script src="{{  asset('/')}}node_modules/chart.js/dist/Chart.js"></script>
         
          <script type="text/javascript">
          	var popCanvas = $("#popChart");
			var popCanvas = document.getElementById("popChart");
			var popCanvas = document.getElementById("popChart").getContext("2d");
          	var barChart = new Chart(popCanvas, {
			  type: 'bar',
			  data: {
			    labels: [<?php 
            foreach( $heads as $key => $row) {
            	$total=0;
            	 foreach($credits as $credit) {
	            	$total += $credit->credit_amount;   
	                }
                   echo "'".$row->head_name.'('.$row->percentage.'%,'.$total.')'."',"; // you can also use $row if you don't use keys                  
            }
          ?>],
			    datasets: [{
			      label: 'Total Credits',
			      data: [<?php 
			      	$total=0;
foreach($heads as $head) {
	$ub_total=0;
            foreach($credits as $credit) {
            	$ub_total += $credit->credit_amount;   
                }
               
                $total=(($ub_total * $head->percentage)/100);
                   echo "'".$total."',"; // you can also use $row if you don't use keys               
        }
          ?>],
			      backgroundColor: [
			        'rgba(255, 99, 132, 0.6)',
			        'rgba(54, 162, 235, 0.6)',
			        'rgba(255, 206, 86, 0.6)',
			        'rgba(75, 192, 192, 0.6)',
			        'rgba(153, 102, 255, 0.6)',
			        'rgba(255, 159, 64, 0.6)',
			        'rgba(255, 99, 132, 0.6)',
			        'rgba(54, 162, 235, 0.6)',
			        'rgba(255, 206, 86, 0.6)',
			        'rgba(75, 192, 192, 0.6)',
			        'rgba(153, 102, 255, 0.6)'
			      ]
			    }]
			  }
			});
          </script>

@endsection