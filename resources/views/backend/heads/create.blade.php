@extends('backend.layouts.master')

@section('content')
<div class="row">
	<div class="col-sm-2">
		
	</div>
	<div class="col-sm-8">
		
	
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Account Head Add</h1>
            </div>

            <div class="card-body">
              <h1 class="text-right"><a href="{{route('head_index')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">
	      	<form method="POST" class="user" action="{{ route('head_store') }}">
	         {{ csrf_field()}}

	          <div class="form-group">
	              <label for="head_name">Account Head Name</label>
	              <div class="form-input">
	                  <input type="text" class="form-control form-control-user is-valid form-control-sm" name="head_name" id="head_name" placeholder="Enter Head Name" value="{{old('head_name')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('head_name')) ? $errors->first('head_name') : ''}}
	                  </div>
	              </div>
	          </div>

	          <div class="form-group">
	              <label for="percentage">Percentage</label>
	              <div class="form-input">
	                  <input type="float" class="form-control form-control-user is-valid form-control-sm" name="percentage" id="percentage" placeholder="Enter Head percentage" value="{{old('percentage')}}" required>
	                  <div class="valid-feedback">
	                    {{ ($errors->has('percentage')) ? $errors->first('percentage') : ''}}
	                  </div>
	              </div>
	          </div>


	          <button class="btn btn-primary float-right" type="submit">Add Head</button>
	      </form>
	    </div>
	  </div>
	</div>
	</div>
	<div class="col-sm-2">
		
	</div>
	</div>
	@endsection