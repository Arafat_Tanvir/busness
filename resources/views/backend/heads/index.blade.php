@extends('backend.layouts.master')

@section('content')
<div class="row">
	
	<div class="col-sm-12">
		
	
<div class="card shadow mb-4">
            <div class="card-header py-3 text-center">
              <h1>Account Head Information</h1>
            </div>

            <div class="card-body">
              <h1 class="text-right"><a href="{{route('head_create')}}"><i class="fas fa-backward"></i></a></h1>
              <div class="mt-2">

              	<div class="table-responsive mt-2">
				        <table id="categories" class="table table-bordered table-striped">
				          <caption>List of Head</caption>
				          <thead>
				  					<tr>
				  						<th>SL</th>
				  						<th>Name</th>
				  						<th>Percentage</th>
				  						<th>Action</th>
				  					</tr>
				  				</thead>
				  				<tbody>
				  					<tr>
				  						<div style="display: none;">{{$a=1}}</div>
				  						@foreach($heads as $head)
				  						<td class="text-center">{{ $a++ }}</td>
				  						<td class="text-center">{{ $head->head_name }}</td>
				  						<td class="text-center">
				  						    @if($head->percentage)
							                  <p>{{ $head->percentage}} %</p>
							                @else
							                    <p>N/A</p>
							                @endif
				  						</td>
				             
				            
				  						<td class="text-center"> <!-- <a href="{{route('head_show', $head->id)}}" class="btn btn-primary btn-sm">Show</a> -->
				  							<a href="{{route('head_edit', $head->id)}}" class="btn btn-warning btn-sm">Edit</a>
				                <a href="#DeleteModal{{ $head->id}}" data-toggle="modal" class="btn btn-danger btn-sm">Delete</a>
												<div class="modal fade" id="DeleteModal{{$head->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<form action="{{ route('head_delete', $head->id)}}" method="POST">
																	{{csrf_field()}}
																<button type="submit" class="badge badge-success">Delete</button>
																</form>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															</div>
														</div>
													</div>
												</div>
											</a>
				  						</td>
				  					</tr>
				  					@endforeach
				  				</tbody>
				        </table>
				      </div>
	      	
	    </div>
	  </div>
	</div>
	</div>
	
	</div>
	@endsection