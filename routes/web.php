<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
	'uses' => 'HomeController@home',
	'as' => '/'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

	//for categories
Route::group(['prefix'=>'heads'],function(){
	Route::get('/',"admin\HeadController@index")->name('head_index');
	Route::get('/create',"admin\HeadController@create")->name('head_create');
	Route::post('/store',"admin\HeadController@store")->name('head_store');
	Route::get('/show/{id}',"admin\HeadController@show")->name('head_show');
	Route::get('/edit/{id}',"admin\HeadController@edit")->name('head_edit');
	Route::post('/update/{id}',"admin\HeadController@update")->name('head_update');
	Route::post('/delete/{id}',"admin\HeadController@delete")->name('head_delete');
});

Route::group(['prefix'=>'credits'],function(){
	Route::get('/',"admin\CreditController@index")->name('credit_index');
	Route::get('/create',"admin\CreditController@create")->name('credit_create');
	Route::post('/store',"admin\CreditController@store")->name('credit_store');
	Route::get('/show/{id}',"admin\CreditController@show")->name('credit_show');
	Route::get('/edit/{id}',"admin\CreditController@edit")->name('credit_edit');
	Route::post('/update/{id}',"admin\CreditController@update")->name('credit_update');
	Route::post('/delete/{id}',"admin\CreditController@delete")->name('credit_delete');
	Route::get('/search_two_date',"admin\CreditController@search_two_date")->name('search_two_date');
	Route::post('/search_bycredit_two_date',"admin\CreditController@search_bycredit_two_date")->name('search_bycredit_two_date');
});

Route::group(['prefix'=>'debits'],function(){
	Route::get('/',"admin\DebitController@index")->name('debit_index');
	Route::get('/create',"admin\DebitController@create")->name('debit_create');
	Route::post('/store',"admin\DebitController@store")->name('debit_store');
	Route::get('/show/{id}',"admin\DebitController@show")->name('debit_show');
	Route::get('/edit/{id}',"admin\DebitController@edit")->name('debit_edit');
	Route::post('/update/{id}',"admin\DebitController@update")->name('debit_update');
	Route::post('/delete/{id}',"admin\DebitController@delete")->name('debit_delete');
	Route::post('/store',"admin\DebitController@store")->name('debit_store');
	Route::get('/search_debit_two_date',"admin\DebitController@search_debit_two_date")->name('search_debit_two_date');
	Route::post('/search_bydebit_two_date',"admin\DebitController@search_bydebit_two_date")->name('search_bydebit_two_date');
});
